export const BASE_API_URL = 'https://jsonplaceholder.typicode.com/';

export const AppRoutes = {
  HOME: '/',
  POSTS_BASE: '/posts',
  POST: (postId: number) => `/posts/${postId}`,
  PROFILES_BASE: '/users',
  PROFILE: (userId: number) => `/users/${userId}`,
};
