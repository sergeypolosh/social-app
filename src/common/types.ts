export type Post = {
  id: number;
  userId: number;
  user?: User;
  title: string;
  body: string;
  comments?: Comment[];
};

export type User = {
  id: number;
  email: string;
  name: string;
  username: string;
  phone: string;
};

export type Comment = {
  id: number;
  postId: number;
  name: string;
  email: string;
  body: string;
};

export type FetchStatus = 'pending' | 'rejected' | 'fulfilled';
