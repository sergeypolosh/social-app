/**
 * Создайте слайс для хранения юзеров
 * При создании обращайте внимание на слайс comments, пусть он будет для вас ориентиром
 * Требования:
 * 1. в стейте слайса хранится массив юзеров, которых мы когда-то подгружали
 * 2. в стейте слайса есть объект со статусами загрузок пользователей по ID
 * 3. в слайсе есть санка на получение пользователя по ID
 * 4. в слайсе есть редюсер, который обрабатывает санку на получение пользователя по ID
 * 5. в слайсе есть селектор на получение пользователя по ID из стейта слайса
 * 6. в слайсе есть селектор на получение статуса загрузки пользователя по ID из стейта
 * 7. слайс экспортирует своих экшены, селекторы и редюсер из слайса
 */
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../..';
import { getSingleUser } from '../../../api';
import { User, FetchStatus } from '../../../common/types';

const initialState = {
  /**
   * 2. Объект со статусами загрузок пользователей по ID
   */
  usersLoadingMap: {} as Record<number, FetchStatus>,
  /**
   * 1. Массив юзеров, которых мы когда-то подгружали
   */
  users: [] as User[],
};

/**
 * 3. Санка на получение пользователя по ID
 */
const fetchUserById = createAsyncThunk<User, number>(
  'users/fetchById',
  async (userId: number): Promise<User> => {
    const user = await getSingleUser(userId);
    return user;
  }
);

export const slice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  /**
   * 4. Редюсер, который обрабатывает санку на получение пользователя по ID
   */
  extraReducers: (builder) => {
    builder.addCase(fetchUserById.pending, (state, { meta }) => {
      state.usersLoadingMap[meta.arg] = 'pending';
    });
    builder.addCase(fetchUserById.rejected, (state, { meta }) => {
      state.usersLoadingMap[meta.arg] = 'rejected';
    });
    builder.addCase(fetchUserById.fulfilled, (state, { meta, payload }) => {
      state.usersLoadingMap[meta.arg] = 'fulfilled';
      state.users = [...state.users, payload];
    });
  },
});

/**
 * 5. Селектор на получение пользователя по ID из стейта слайса
 */
const selectUserById = (userId: number) => (state: RootState) =>
  state.users.users.find((user) => user.id === userId) as User;

/**
 * 6. Селектор на получение статуса загрузки пользователя по ID из стейта
 */
const selectUserLoadingStatus = (userId: number) => (state: RootState) =>
  state.users.usersLoadingMap[userId];

/**
 * 7. Слайс экспортирует своих экшены, селекторы и редюсер из слайса
 */
export const actions = { ...slice.actions, fetchUserById };
export const reducer = slice.reducer;
export const selectors = { selectUserById, selectUserLoadingStatus };
