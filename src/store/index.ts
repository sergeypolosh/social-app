import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from './slices';

/**
 * Наш стор, чтобы его создать нужно передать редюсер.
 * В подходе со слайсами главное передать редюсер, который комбинирует все редюсеры
 *  из слайсов
 */
export const store = configureStore({ reducer: rootReducer });

/**
 * Тип состояния, хранящегося в сторе.
 * Можем использовать его в селекторах, чтобы знать какие данные приходят в стейте
 */
export type RootState = ReturnType<typeof store.getState>;

export { actions } from './slices';
export { selectors } from './slices';
